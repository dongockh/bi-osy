#include <vector>
#include <thread>
#include <iostream>
#include <pthread.h>
#include <math.h>

#define CORE_COUNT 2

using namespace std;

struct thread_data
{
  int thread_id;
  int start;
  int end;
};

int countMembers = 30000;

long double sum = 0;

void *threadFunc(void *threadarg)
{
  struct thread_data *my_data;
  my_data = (struct thread_data *)threadarg;
  // cout << my_data->thread_id << " " << my_data->start << " " << my_data->end << endl;
  for (int i = my_data->start; i < (my_data->end); ++i)
  {
    sum += (((sqrtl(i + 1) + i) / sqrtl((i * i) + i + 1)));
    cout << sum << endl;
  }
  // cout << "Thread : " << my_data->thread_id << " stop" << endl;
  return NULL;
}

int main()
{
  struct thread_data thread_data_array[CORE_COUNT];
  pthread_t threads[CORE_COUNT];
  int i, rc;
  int fromTo[CORE_COUNT + 1];

  // cout << "Main : start" << endl;

  int spacing = countMembers / CORE_COUNT;

  for (int i = 0; i <= CORE_COUNT; i++)
  {
    fromTo[i] = i * spacing;
    // cout << fromTo[i] << endl;
  }

  for (i = 0; i < CORE_COUNT; i++)
  {
    thread_data_array[i].thread_id = i;
    thread_data_array[i].start = fromTo[i];
    thread_data_array[i].end = fromTo[i + 1];
    rc = pthread_create(&threads[i], NULL, threadFunc, (void *)&thread_data_array[i]); /* <--- #6 */
    if (rc)
    {
      printf("ERROR; return code from pthread_create() is %d\n", rc);
      return EXIT_FAILURE;
    }
  }

  for (i = 0; i < CORE_COUNT; i++)
  {
    rc = pthread_join(threads[i], NULL); /* <--- #4 */
    if (rc)
    {
      printf("ERROR return code from pthread_join() is %d\n", rc);
      return EXIT_FAILURE;
    }
  }

  // cout << sum << endl;

  // cout << "Main : stop" << endl;

  return 0;
}
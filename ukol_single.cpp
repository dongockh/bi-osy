#include <iostream>
#include <math.h>

using namespace std;

int main(void)
{
	long double sum = 0;
	for (long double i = 0; i < 30000; i++)
	{
		sum += ((pow(i + 1, 0.5) + i) / (pow((i * i) + i + 1, 0.5)));
		cout << sum << endl;
	}

	return 0;
}